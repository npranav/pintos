#ifndef VM_SWAP_TABLE_H
#define VM_SWAP_TABLE_H

#include "devices/block.h"
#include "vm/frame.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include <list.h>
#include <bitmap.h>
#include "threads/thread.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "threads/palloc.h"

struct swap_slot
{
	struct thread *owner;
	struct virtual_page *page;
	block_sector_t sector_start;

	struct list_elem elem;
};

void swap_table_init ();
struct swap_slot *store_frame (struct frame *);
bool retrieve_frame (struct swap_slot *, struct frame *);
void print_swap_stats ();

#endif
