#!/bin/bash	
result_file="test_results"
number_of_threads=4
echo "writing result_file"
echo "# -*- perl -*-" > $result_file
echo -e "use strict;\nuse warnings;\nuse tests::tests;\ncheck_expected (IGNORE_EXIT_CODES => 1, [<<'EOF']);" >> $result_file
echo "(page-parallel) begin" >> $result_file
i=1
while [ $i -le $number_of_threads ]; do
	echo "(page-parallel) exec \"child-linear\"" >> $result_file
	(( i++ ))
done
i=1
while [ $i -le $number_of_threads ]; do
	echo "(page-parallel) wait for child $(( i - 1 ))" >> $result_file
	(( i++ ))
done
echo -e "(page-parallel) end\nEOF\npass;" >> $result_file
echo "done..."
