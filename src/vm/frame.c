#include "vm/frame.h"

static struct list frame_table;
static struct lock frame_table_lock;
static int frame_count;

void
frame_table_init ()
{
  printf ("initializing frame table ...\n");
  list_init (&frame_table);
  printf ("initializing frame_table_lock... \n");
  lock_init (&frame_table_lock);
  frame_count = 0;
}

struct frame *
create_frame (struct virtual_page *page)
{
  void *k_page = palloc_get_page (PAL_USER | PAL_ZERO);

  if (k_page == NULL)
    return evict_frame (page); // for eviction

  struct frame *f = (struct frame *) malloc (sizeof (struct frame));
  if (f)
  {
    f->owner = thread_current ();
    f->page = page;
    f->k_page = k_page;

    f->pin = false;
    f->thief = NULL;

    return f;
  }
  return NULL;
}

void
add_frame (struct frame *f)
{
  ASSERT (f != NULL);
  lock_acquire (&frame_table_lock);
  list_push_back (&frame_table, &f->elem);
  frame_count++;
  lock_release (&frame_table_lock);
}

bool
remove_frame (struct frame *f)
{
  ASSERT (f != NULL);
  bool remove_status = false;
  lock_acquire (&frame_table_lock);
  if (!f->pin || (f->thief == thread_current ()))
  {
    list_remove (&f->elem);
    frame_count--;
    remove_status = true;
  }
  lock_release (&frame_table_lock);
  return remove_status;
}

void
destroy_frame (struct frame *f)
{
  ASSERT (f != NULL);
  palloc_free_page (f->k_page);
  free (f);
}

void
free_frame_table ()
{
  while (!list_empty (&frame_table))
  {
    
  }
}

void
print_frame_stats ()
{
  printf ("frame usage : %d\n", frame_count);
  printf ("frame table size : %d\n", list_size (&frame_table));
}

struct frame *
evict_frame (struct virtual_page *p)
{
  /*
    1.  get frame table lock
    2.  get a frame to evict
    3.  release frame table lock
    4.  store the frame if dirty
    5.  get page table lock of the owner
    6.  clear the page
    7.  release the page table lock of the owner
    8.  clean up the frame and return it
  */

  lock_acquire (&frame_table_lock);
  struct frame *f_stolen = get_random_frame ();
  frame_count--;
  lock_release (&frame_table_lock);

  if (f_stolen == NULL)
    PANIC ("no frame to evict!\n");

  struct virtual_page *p_stolen = f_stolen->page;
  struct thread *t_owner = p_stolen->owner;
  //printf ("owner tid : %d, thread_current tid : %d\n", t_owner->tid, thread_tid ());

  if (is_alive (t_owner) && p_stolen != NULL)
  {
    ASSERT (is_thread (t_owner));
    lock_acquire (&t_owner->page_table_lock);
    if (t_owner->pagedir != NULL && p_stolen->free_on_evict == false)
      pagedir_clear_page (t_owner->pagedir, p_stolen->v_page);
    struct swap_slot *slot = NULL;
    if (p_stolen->free_on_evict == false && pagedir_is_dirty (t_owner->pagedir, f_stolen->k_page))
    {
      if (p_stolen->page_type == DATA || p_stolen->page_type == STACK)
        slot = store_frame (f_stolen);
      else if (p_stolen->page_type == MMAP && is_file_writable (p_stolen->page_file))
      {
        get_filesys_lock ();
        int written = file_write_at (p_stolen->page_file, f_stolen->k_page, \
                                      p_stolen->page_read_bytes, p_stolen->file_ofs);
        ASSERT (written == p_stolen->page_read_bytes);
        release_filesys_lock ();
      }
    }

    if (p_stolen->free_on_evict != true && is_alive (t_owner))
    {
      p_stolen->page_frame = NULL;
      p_stolen->page_slot = slot;
    }
    else if (slot != NULL)
    {
      retrieve_frame (slot, NULL);
    }

    if (p_stolen->free_on_evict)
    {
      list_remove (&p_stolen->elem);
      free (p_stolen);
    }
    lock_release (&t_owner->page_table_lock);
  }

  f_stolen->owner = thread_current ();
  f_stolen->pin = false;
  f_stolen->page = p;
  f_stolen->thief = NULL;
  memset (f_stolen->k_page, 0, PGSIZE);

  return f_stolen;
}

struct frame *
get_random_frame ()
{
  struct list_elem *e = NULL;
  struct frame *target = NULL;
  uint32_t attempts = frame_count;
  
  for (e = list_begin (&frame_table); e != list_end (&frame_table);
    e = list_next (e))
  {
    target = list_entry (e, struct frame, elem);
    if (!target->pin)
    {
      list_remove (&target->elem);
      target->pin = true;
      target->thief = thread_current ();
      return target;
    }
    else
      target = NULL;
  }
  return target;
}

struct thread *
pin_frame (struct frame *f)
{
  if (f == NULL)
    return NULL;

  lock_acquire (&frame_table_lock);
  if (f->pin == false && f->thief == NULL)
  {
    f->pin = true;
    f->thief = thread_current ();
  }
  lock_release (&frame_table_lock);

  return f->thief;
}

void
unpin_frame (struct frame *f)
{
  lock_acquire (&frame_table_lock);
  if (f != NULL && f->thief == thread_current () && f->pin == true)
  {
    f->pin = false;
    f->thief = NULL;
  }
  lock_release (&frame_table_lock);
}
