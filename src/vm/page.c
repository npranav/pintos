#include "vm/page.h"

static bool
page_order_function (struct list_elem *a, struct list_elem *b, void *aux)
{
  struct virtual_page *page_a = list_entry (a, struct virtual_page, elem);
  struct virtual_page *page_b = list_entry (b, struct virtual_page, elem);

  if (page_a->v_page < page_b->v_page)
    return true;

  return false;
}

struct virtual_page *
add_page (void *v_page, page_type_t page_type, struct frame *cur_f, off_t file_ofs, 
  uint32_t page_read_bytes, bool writable, struct file *page_file)
{
  struct thread *t = thread_current ();
  if (is_valid_page (v_page, NULL) != NULL)
    return NULL;

  struct virtual_page *p = (struct virtual_page *) malloc (sizeof (struct virtual_page));

  if (p)
  {
    p->v_page = v_page;
    p->page_type = page_type;
    p->page_frame = cur_f;
    p->file_ofs = file_ofs;
    p->page_read_bytes = page_read_bytes;
    p->writable = writable;
    p->page_file = page_file;
    p->owner = t;
    p->page_slot = NULL;
    p->free_on_evict = false;
    //printf ("tid : %d adding page : %p, file : %p\n", t->tid, v_page, page_file);
    lock_acquire (&t->page_table_lock);
    list_insert_ordered (&t->page_table, &p->elem, page_order_function, NULL);
    lock_release (&t->page_table_lock);

    return p;
  }

  return NULL;
}

struct virtual_page *
is_valid_page (void *address, struct thread *t)
{
  if (t == NULL)
    t = thread_current ();

  struct list_elem *e = NULL;
  void *page_start = pg_round_down (address);

  for (e = list_begin (&t->page_table); e != list_end (&t->page_table); e = list_next (e))
  {
    struct virtual_page *p = list_entry (e, struct virtual_page, elem);
    if (p->v_page == page_start)
      return p;
  }
  return NULL;
}

bool
page_install (struct virtual_page *p, struct frame *f)
{
  ASSERT (f != NULL);
  struct thread *t = thread_current ();
  bool status = false;
  lock_acquire (&t->page_table_lock);
  status = pagedir_get_page (t->pagedir, p->v_page) == NULL
          && pagedir_set_page (t->pagedir, p->v_page, f->k_page, p->writable);
  if (status)
    p->page_frame = f;
  lock_release (&t->page_table_lock);
  //printf ("tid : %d, page_install status : %d, page : %p\n", t->tid, status, p->v_page);
  return status;
}

void
page_uninstall (struct virtual_page *p, struct thread *t, bool remove)
{
  ASSERT (p->owner == t);

  lock_acquire (&t->page_table_lock);
  pagedir_clear_page (t->pagedir, p->v_page);
  if (remove)
    list_remove (&p->elem);
  lock_release (&t->page_table_lock);
}

bool
handle_page (struct virtual_page *p, struct frame *f)
{
  ASSERT (p != NULL);
  ASSERT (f != NULL);
  struct thread *t = thread_current ();

  //if (p->page_slot == NULL && p->page_file != NULL)
  if (p->page_slot != NULL)
  {
    if (retrieve_frame (p->page_slot, f))
    {
      p->page_slot = NULL;
      return true;
    }
  }
  else
  {
    if (p->page_file != NULL)
    {
      get_filesys_lock ();
      int bytes_read = file_read_at (p->page_file, f->k_page, p->page_read_bytes, p->file_ofs);
      release_filesys_lock ();
      if (bytes_read == p->page_read_bytes)
        return true;
    }
    else if (p->page_type == STACK)
    {
      return true; // STACK PAGE, nothing to read
    }
  }
  return false;
}

bool
handle_page_fault (struct intr_frame *f, void *fault_addr)
{
  struct thread *t = thread_current ();
  struct virtual_page *cur_p = is_valid_page (fault_addr, NULL);
  if (cur_p)
  {
    struct frame *cur_f = create_frame (cur_p);
    if (cur_f != NULL)
    {
      if (page_install (cur_p, cur_f))
      {
        if (handle_page (cur_p, cur_f))
        {
          add_frame (cur_f);
          return true;
        }
        else
        {
          page_uninstall (cur_p, t, false);
        }
      }
      else
      {
        destroy_frame (cur_f);
      }
    }
    return false;
  }
  else if (f != NULL && check_stack_access (f, fault_addr))
    return handle_page_fault (f, fault_addr);

  return false;
}

//The page must have been removed from the thread's page_table
static void
free_page (struct virtual_page *cur_p)
{
  ASSERT (cur_p != NULL);
  struct thread *t = thread_current ();
  void *page_buffer = NULL;
  bool destroy_page = true;

  if (cur_p->page_file != NULL && is_file_writable (cur_p->page_file)
    && pagedir_is_dirty (t->pagedir, cur_p->v_page))
  {
    if (pin_page (cur_p))
    {
      ASSERT (cur_p->page_frame != NULL);
      //printf ("tid : %d writing to file %p\n", t->tid, cur_p->page_file);
      page_buffer = cur_p->page_frame->k_page;
      get_filesys_lock ();
      int written = file_write_at (cur_p->page_file, page_buffer, cur_p->page_read_bytes, cur_p->file_ofs);
      ASSERT (written == cur_p->page_read_bytes);
      release_filesys_lock ();
      unpin_page (cur_p);
    }
  }
  else if (cur_p->page_slot != NULL)
    retrieve_frame (cur_p->page_slot, NULL);

  if (cur_p->page_frame != NULL)
  {
    if (remove_frame (cur_p->page_frame))
    {
      destroy_frame (cur_p->page_frame);
      destroy_page = true;
    }
    else
      destroy_page = false;
  }

  page_uninstall (cur_p, t, destroy_page);
  ASSERT (pagedir_get_page (t->pagedir, cur_p->v_page) == NULL);

  if (destroy_page)
    free (cur_p);
}

void
free_page_table ()
{
  struct thread *t = thread_current ();
  while (!list_empty (&t->page_table))
  {
    struct list_elem *e = list_front (&t->page_table);
    struct virtual_page *p = list_entry (e, struct virtual_page, elem);

    free_page (p);
  }
}

void
munmap_write_files (struct file *munmap_file)
{
  struct thread *t = thread_current ();
  struct list_elem *i = list_begin (&t->page_table);
  //printf ("unmapping file : %p\n", munmap_file);

  while (i != list_end (&t->page_table))
  {
    struct virtual_page *p = list_entry (i, struct virtual_page, elem);
    //printf ("checking page : %p, file : %p\n", p->v_page, p->page_file);
    if (p->page_file == munmap_file)
    {
      i = list_next (i);
      free_page (p);
      continue;
    }
    //printf ("input_file : %p, page_file : %p\n", munmap_file, p->page_file);
    i = list_next (i);
  }
}

bool
add_stack_pages (void *page_start, struct intr_frame *f)
{
  int stack_pages = 0;
  void *stack = page_start;

  while (!is_valid_page (stack, NULL))
  {
    stack_pages++;
    stack += PGSIZE;
  }
  //printf ("number of stack_pages : %d, page_start : %p, current_stack : %p\n", stack_pages, page_start, stack);
  int added_stack_pages = 0;
  stack = page_start;
  while (added_stack_pages < stack_pages)
  {
    struct virtual_page *p = add_page (stack, STACK, NULL, -1, -1, true, NULL);
    if (p == NULL)
      return false;
    added_stack_pages++;
    stack += PGSIZE;
  }
  return true;
}

bool
check_stack_access (struct intr_frame *f, void *fault_addr)
{
  struct thread *t = thread_current ();
  void *page_start = pg_round_down (fault_addr);
  void *esp = f->esp;

  //printf ("fault_addr : %p, t->stack : %p, for tid : %d\n", fault_addr, t->stack, t->tid);
  if (!is_user_vaddr (fault_addr) ||
          (((uintptr_t)esp - (uintptr_t)fault_addr) > 32) ||
          ((PHYS_BASE - page_start) > STACK_MAX_SIZE))
  {
    return false;
  }

  if (add_stack_pages (page_start, f))
  {
    //printf ("page added for fault_addr (page_start) : %p (%p), f->esp : %p, for tid : %d\n", \
      fault_addr, page_start, f->esp, t->tid);
    return true;
  }
  return false;
}

bool
pin_page (struct virtual_page *p)
{
  struct thread *t = thread_current ();

  do
  {
    if (p->page_frame == NULL)
    {
      if (!handle_page_fault (NULL, p->v_page))
        return false;
    }
    //printf ("going in loop...\n");
  }  while (pin_frame (p->page_frame) != t);

  //printf("pin_page returning true...\n");
  return true;
}

void
unpin_page (struct virtual_page *p)
{
  if (p)
    unpin_frame (p->page_frame);
}
