#!/bin/bash
MY_MESSAGE="page-parallel new test..."
echo $MY_MESSAGE
number_of_threads=13
while [ $number_of_threads -le 16 ]; do
	echo -e "============================================================================================="
	echo -e "TESTING number_of_threads = " $number_of_threads
	sed -i "7s/.*/#define CHILD_CNT $number_of_threads/" ../tests/vm/page-parallel.c
	echo `pwd`
	make clean all &> /dev/null
	echo "BUILD directory modification timestamp : " `stat -c %y build` "sleeping for 1 second"

	result_file="cur_test_result"
	echo "writing result_file"
	echo "# -*- perl -*-" > $result_file
	echo -e "use strict;\nuse warnings;\nuse tests::tests;\ncheck_expected (IGNORE_EXIT_CODES => 1, [<<'EOF']);" >> $result_file
	echo "(page-parallel) begin" >> $result_file
	i=1
	while [ $i -le $number_of_threads ]; do
		echo "(page-parallel) exec \"child-linear\"" >> $result_file
		(( i++ ))
	done
	i=1
	while [ $i -le $number_of_threads ]; do
		echo "(page-parallel) wait for child $(( i - 1 ))" >> $result_file
		(( i++ ))
	done
	echo -e "(page-parallel) end\nEOF\npass;" >> $result_file
	echo "done writing rsult file for " $number_of_threads " threads"

	cd build
	iteration=1
	echo `pwd`
	while [ $iteration -le 100 ]; do
		SWAP_SPACE=`shuf -i $number_of_threads-24 -n 1`
		pintos -v -k -T 60 --qemu  --filesys-size=2 -p tests/vm/page-parallel -a page-parallel -p tests/vm/child-linear -a child-linear --swap-size=$SWAP_SPACE -- -q  -f run page-parallel < /dev/null 2> tests/vm/page-parallel.errors > tests/vm/page-parallel.output
		out=`perl -I../.. ../$result_file tests/vm/page-parallel tests/vm/page-parallel.result`
		echo -e "iteration : " $iteration "\toutput :" $out "\tSWAP_SPACE : " $SWAP_SPACE
		mv tests/vm/page-parallel.output tests/vm/page-parallel.output.$iteration
		(( iteration++ ))
	done
	cd ..
	mv build archived_builds/build.$number_of_threads
	(( number_of_threads++ ))
done