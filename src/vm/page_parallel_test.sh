#!/bin/bash

# FOR individual thread count tests

MY_MESSAGE="Hello World"
echo $MY_MESSAGE
cd build
iteration=1
while [ $iteration -le 100 ]; do
	SWAP_SPACE=`shuf -i 4-8 -n 1`
	pintos -v -k -T 60 --qemu  --filesys-size=2 -p tests/vm/page-parallel -a page-parallel -p tests/vm/child-linear -a child-linear --swap-size=$SWAP_SPACE -- -q  -f run page-parallel < /dev/null 2> tests/vm/page-parallel.errors > tests/vm/page-parallel.output
	out=`perl -I../.. ../../tests/vm/page-parallel.ck tests/vm/page-parallel tests/vm/page-parallel.result`
	echo -e "iteration : " $iteration "\toutput :" $out "\tSWAP_SPACE : " $SWAP_SPACE
	mv tests/vm/page-parallel.output tests/vm/page-parallel.output.$iteration
	(( iteration++ ))
done
# cd ..