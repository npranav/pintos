#include "vm/swap.h"

static struct list swap_table;
static struct lock swap_table_lock;
static struct block *swap_block;

static block_sector_t total_number_of_sectors;
static block_sector_t sectors_per_page;
static uint32_t total_pages_in_swap;
static struct bitmap *swap_map;
static int swap_usage;

void
swap_table_init ()
{
  list_init (&swap_table);
  lock_init (&swap_table_lock);

  swap_block = block_get_role (BLOCK_SWAP);
  total_number_of_sectors = block_size (swap_block);
  sectors_per_page = PGSIZE / BLOCK_SECTOR_SIZE;
  //printf ("%d sectors available in swap.\n", total_number_of_sectors);
  total_pages_in_swap = total_number_of_sectors / sectors_per_page;
  printf ("%d pages available in swap.\n", total_pages_in_swap);
  swap_map = bitmap_create (total_pages_in_swap);
  swap_usage = 0;
}

struct swap_slot *
store_frame (struct frame *cur_frame)
{
  struct swap_slot *cur_slot = (struct swap_slot *) malloc 
                                (sizeof (struct swap_slot));
  if (cur_slot && cur_frame)
  {
    lock_acquire (&swap_table_lock);
    block_sector_t i = 0;
    while ((i < total_pages_in_swap) && bitmap_test (swap_map, i))
      i++;
    if (i >= total_pages_in_swap)
    {
      lock_release (&swap_table_lock);
      PANIC ("tid %d : swap is full...\n", thread_tid ());
    }
    bitmap_mark (swap_map, i);
    swap_usage++;
    lock_release (&swap_table_lock);
    
    cur_slot->owner = cur_frame->owner;
    cur_slot->page = cur_frame->page;

    i = i * sectors_per_page;
    cur_slot->sector_start = i;

    void *buf = calloc (BLOCK_SECTOR_SIZE, 1);
    ASSERT (buf != NULL);
    for (block_sector_t j = 0; j < sectors_per_page; j++)
    {
      memcpy (buf, cur_frame->k_page + j * BLOCK_SECTOR_SIZE, BLOCK_SECTOR_SIZE);
      block_write (swap_block, i + j, buf);
    }
    free (buf);
    return cur_slot;
  }
  return NULL;
}

bool
retrieve_frame (struct swap_slot *slot, struct frame *my_frame)
{
  ASSERT (slot != NULL);

  if (my_frame)
  {
    ASSERT (slot->owner == my_frame->owner);
    ASSERT (slot->page->v_page == my_frame->page->v_page);
    block_sector_t i = 0;
    for (i = 0; i < sectors_per_page; i++)
      block_read (swap_block, i + slot->sector_start, my_frame->k_page + i * BLOCK_SECTOR_SIZE);
    //printf ("slot sector start : %d, i : %d\n", slot->sector_start, i);
  }

  lock_acquire (&swap_table_lock);
  bitmap_reset (swap_map, (slot->sector_start / sectors_per_page));
  free (slot);
  swap_usage--;
  lock_release (&swap_table_lock);
  return true;
}

void
print_swap_stats ()
{
  printf ("swap usage : %d/%d\n", swap_usage, total_pages_in_swap);
}
