#ifndef VM_FRAME_H
#define VM_FRAME_H

#include <list.h>
#include <random.h>
#include <string.h>
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "vm/page.h"
#include <stdbool.h>
#include <stdint.h>

struct frame
{
	struct thread *owner;
	struct virtual_page *page;
	void *k_page;
	
	bool pin;
	struct thread *thief;

	struct list_elem elem;
};

void frame_table_init ();

struct frame *create_frame (struct virtual_page *v_page);
void add_frame (struct frame *);
bool remove_frame (struct frame *);
void destroy_frame (struct frame *);

void free_frame_table ();
void print_frame_stats ();

struct frame *evict_frame (struct virtual_page *);
struct frame *get_random_frame ();

struct thread *pin_frame (struct frame *);
void unpin_frame (struct frame *);

#endif