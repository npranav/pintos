#ifndef VM_PAGE_H
#define VM_PAGE_H

#include "threads/synch.h"
#include "threads/thread.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "filesys/file.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "userprog/syscall.h"
#include "threads/interrupt.h"
#include <list.h>
#include "vm/frame.h"
#include "vm/swap.h"

#include <stdbool.h>
#include <stdint.h>

#define STACK_MAX_SIZE 0x800000

typedef enum _page_type
{
  TEXT,
  DATA,
  STACK,
  MMAP
} page_type_t;

struct virtual_page
{
  void *v_page;
  page_type_t page_type;
  struct thread *owner;

  struct frame *page_frame;
  struct swap_slot *page_slot;

  struct file *page_file;
  off_t file_ofs;
  uint32_t page_read_bytes;
  bool writable;

  bool free_on_evict;

  struct list_elem elem;
};


struct virtual_page *add_page (void *, page_type_t, struct frame *, off_t file_ofs, 
	uint32_t page_read_bytes, bool writable, struct file *);

struct virtual_page *is_valid_page (void *, struct thread *);
bool handle_page_fault (struct intr_frame *, void *);
bool handle_page (struct virtual_page *, struct frame *);
bool page_install (struct virtual_page *, struct frame *);
void page_uninstall (struct virtual_page *, struct thread *, bool);

void free_page_table ();
void munmap_write_files (struct file *);

bool check_stack_access (struct intr_frame *, void *);
bool add_stack_pages (void *, struct intr_frame *);

bool pin_page (struct virtual_page *);
void unpin_page (struct virtual_page *);

#endif
