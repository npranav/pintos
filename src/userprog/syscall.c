#include "userprog/syscall.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include <stdio.h>
#include <syscall-nr.h>
#include <list.h>
#include <string.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "devices/shutdown.h"
#include "filesys/filesys.h"
#include "threads/malloc.h"
#include "vm/page.h"

void syscall_halt (struct intr_frame *);
void syscall_exit (struct intr_frame *);
void syscall_exec (struct intr_frame *);
void syscall_wait (struct intr_frame *);
void syscall_create (struct intr_frame *);
void syscall_remove (struct intr_frame *);
void syscall_open (struct intr_frame *);
void syscall_filesize (struct intr_frame *);
void syscall_read (struct intr_frame *);
void syscall_write (struct intr_frame *);
void syscall_seek (struct intr_frame *);
void syscall_tell (struct intr_frame *);
void syscall_close (struct intr_frame *);
void syscall_mmap (struct intr_frame *);
void syscall_munmap (struct intr_frame *);

static void syscall_handler (struct intr_frame *);
static struct lock filesys_lock;
typedef int mapid_t;

static struct thread_fd
{
  int t_fd;
  mapid_t mmap_id;
  struct file *t_file;
  struct list_elem elem;
};

void
lock_file (struct file *cur)
{
  if (cur && cur != NULL)
    file_deny_write (cur);
}

static struct thread_fd *
get_fd (struct thread *t, int fd)
{
  struct thread_fd *cur = 0;
  struct list_elem *e;
  for (e = list_begin (&t->fd_list); e != list_end (&t->fd_list);
      e = list_next (e))
  {
    cur = list_entry (e, struct thread_fd, elem);
    if (cur->t_fd == fd || cur->mmap_id == fd)
      break;
    else
      cur = 0;
  }
  return cur;
}

static struct file *
get_file_from_fd (struct thread *t, int fd)
{
  struct thread_fd *cur = get_fd (t, fd);
  if (cur)
    return cur->t_file;

  return 0;
}

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init (&filesys_lock);
}

void
get_filesys_lock ()
{
  lock_acquire (&filesys_lock);
}

void
release_filesys_lock ()
{
  lock_release (&filesys_lock);
}

static bool
is_valid_pointer (void *ptr)
{
  if (ptr != NULL && is_user_vaddr (ptr) && is_valid_page (ptr, NULL) != NULL)
    return true;
  return false;
}

void
exit (int status)
{
  set_child_exit_status (status);
  thread_exit ();
}

void
syscall_exit (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int status = -1;

  if (is_valid_pointer (sp))
  {
    status = *((int *)sp);
    exit (status);
  }
  exit (-1);
}

void
halt ()
{
  shutdown_power_off ();
}

void
syscall_halt (struct intr_frame *f)
{
  halt ();
}

int
exec (const char *cmd_line)
{
  return process_execute (cmd_line);
}

void
syscall_exec (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  void *cmd_line;

  if (is_valid_pointer (sp))
  {
    cmd_line = *((char **)sp);
    if (is_valid_pointer (cmd_line))
      f->eax = exec (cmd_line);
    return;
  }
  exit (-1);
}

int
wait (int pid)
{
  return process_wait (pid);
}

void
syscall_wait (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int child_pid = -1;

  if (is_valid_pointer (sp))
  {
    child_pid = *((int *)sp);
    f->eax = wait (child_pid);
  }
  else
  {
    f->eax = -1;
  }
}

bool
create (const char *file, unsigned initial_size)
{
  bool _return = false;

  lock_acquire (&filesys_lock);
  if (file != NULL && strlen (file) > 0)
    _return = filesys_create (file, initial_size);

  lock_release (&filesys_lock);
  return _return;
}

void
syscall_create (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  void *file = 0;
  unsigned filesize = 0;

  bool _return = false;

  if (is_valid_pointer (sp))
  {
    file = *((char **)sp);
    sp += sizeof (int);

    if (is_valid_pointer (file) && is_valid_pointer (sp))
    {
      filesize = *((int *) sp);
      f->eax = create (file, filesize);
      return;
    }
  }
  exit (-1);
}

bool
remove (const char *file)
{
  bool _return = false;
  lock_acquire (&filesys_lock);
  _return = filesys_remove (file);
  lock_release (&filesys_lock);
  return _return;
}

void
syscall_remove (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  void *file = 0;
  bool _return = false;

  if (is_valid_pointer (sp))
  {
    file = *((char **)sp);
    if (is_valid_pointer (file) && file != NULL && strlen (file) > 0)
      _return = remove (file);
    else
      exit (-1);
  }
  f->eax = _return;
}

int
open (const char *file)
{
  struct thread *t = thread_current ();
  int cur_fd = -1;
  struct file *cur_file = 0;

  lock_acquire (&filesys_lock);
  if (is_valid_pointer (file))
  {
    cur_file = filesys_open (file);
    if (cur_file)
    {
      cur_fd = t->new_fd;
      struct thread_fd *this_fd = (struct thread_fd *)(malloc (sizeof (struct thread_fd)));
      this_fd->t_fd = cur_fd;
      this_fd->t_file = cur_file;
      this_fd->mmap_id = -1;

      t->new_fd = cur_fd + 1;
      list_push_back (&t->fd_list, &this_fd->elem);
      file_allow_write (cur_file);
    }
  }
  lock_release (&filesys_lock);

  return cur_fd;
}

void
syscall_open (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  void *file = 0;
  if (is_valid_pointer (sp))
  {
    file = *((char **)sp);
    if (is_valid_pointer (file))
    {
      f->eax = open (file);
      return;
    }
  }
  exit (-1);
}

int
filesize (int fd)
{
  int cur_filesize = 0;

  struct thread *t = thread_current ();
  lock_acquire (&filesys_lock);
  struct file *cur = get_file_from_fd (t, fd);
  if (cur)
  {
    cur_filesize = file_length (cur);
  }
  lock_release (&filesys_lock);
  return cur_filesize;
}

void
syscall_filesize (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    f->eax = filesize (fd);
    return;
  }
  exit (-1);
}

int
read (int fd, void *buffer, unsigned size)
{
  struct thread *t = thread_current ();
  struct file *cur_file = get_file_from_fd (t, fd);
  int bytes_read = -1;
  if (cur_file)
  {
    lock_acquire (&filesys_lock);
    bytes_read = file_read (cur_file, buffer, size);
    lock_release (&filesys_lock);
  }
  return bytes_read;
}

void
syscall_read (struct intr_frame *f)
{
	struct thread *t = thread_current ();
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  char *read_buf;
  char *read_buf_pin;
  unsigned size;

  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    sp += sizeof (int);
    if (is_valid_pointer (sp))
    {
      read_buf = *((char **)sp);
      read_buf_pin = read_buf;
      sp += sizeof (char *);
      //printf ("read_buf : %p, size : %d, f->esp : %p\n", read_buf, *(int *)sp, f->esp);
      struct virtual_page *read_buf_page = is_valid_page (read_buf, NULL);
      //printf ("read_buf_page : %p\n", read_buf_page);

      if (is_valid_pointer (read_buf) && read_buf_page
      && read_buf_page->writable && is_valid_pointer (sp))
      {
        size = *((int *)sp);

        int pin_size = size + ((uintptr_t) read_buf - (uintptr_t) read_buf_page->v_page);
        unsigned pinned_pages = 0;
        while (pin_size > 0)
        {
          if (read_buf_page && read_buf_page->writable && pin_page (read_buf_page))
          {
            pin_size -= PGSIZE;
            read_buf_pin += PGSIZE;
            pinned_pages++;
            read_buf_page = is_valid_page (read_buf_pin, NULL);
          }
          else
           exit (-1);
        }

        if (fd == 0)
        {
          int i = 0;
          while (i <= size)
          {
            read_buf[i] = input_getc ();
            i++;
          }
          f->eax = i;
        }
        else
        {
          f->eax = read (fd, read_buf, size);
        }

        while (pinned_pages--)
        {
          unpin_page (is_valid_page (read_buf_pin, NULL));
          read_buf_pin -= PGSIZE;
        }

        return;
      }
    }
  }
  exit(-1);
}

int
write (int fd, const void *buffer, unsigned size)
{
  struct thread *t = thread_current ();
  struct file *cur_file = get_file_from_fd (t, fd);
  int written = 0;
  if (cur_file)
  {
    lock_acquire (&filesys_lock);
    written = file_write (cur_file, buffer, size);
    lock_release (&filesys_lock);
  }
  return written;
}

void
syscall_write (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd = -1;
  char *write_buf;
  unsigned size = -1;

  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    sp += sizeof (int);

    if (is_valid_pointer (sp))
    {
      write_buf = *((char **)sp);
      char *write_buf_pin = write_buf;
      sp += sizeof (char *);

      struct virtual_page *write_buf_page = is_valid_page (write_buf, NULL);

      if (is_valid_pointer (write_buf) && write_buf_page
        && is_valid_pointer (sp))
      {
        size = *((int *)sp);

        int pin_size = size + ((uintptr_t) write_buf - (uintptr_t) write_buf_page->v_page);
        unsigned pinned_pages = 0;
        while (pin_size > 0)
        {
          if (write_buf_page && pin_page (write_buf_page))
          {
            pin_size -= PGSIZE;
            write_buf_pin += PGSIZE;
            pinned_pages++;
            write_buf_page = is_valid_page (write_buf_pin, NULL);
          }
          else
           exit (-1);
          //printf ("pin_size : %d\n", pin_size);
        }

        if (fd == 1)
        {
          putbuf (write_buf, size);
          f->eax = size;
        }
        else
        {
          f->eax = write (fd, write_buf, size);
        }

        while (pinned_pages--)
        {
          unpin_page (is_valid_page (write_buf_pin, NULL));
          write_buf_pin -= PGSIZE;
        }

        return;
      }
    }
  }
  exit (-1);
}

void
seek (int fd, unsigned position)
{
  struct thread *t = thread_current ();
  struct file *cur = get_file_from_fd (t, fd);

  lock_acquire (&filesys_lock);
  if (cur && position >= 0)
    file_seek (cur, position);
  lock_release (&filesys_lock);
}

void
syscall_seek (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  unsigned position;

  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    sp += sizeof (int);
    if (is_valid_pointer (sp))
    {
      position = *((int *)sp);
      seek (fd, position);
      return;
    }
  }
  exit (-1);
}

unsigned
tell (int fd)
{
  unsigned pos = 0;
  struct thread *t = thread_current ();
  lock_acquire (&filesys_lock);
  struct file *cur = get_file_from_fd (t, fd);
  if (cur)
    pos = file_tell (cur);
  lock_release (&filesys_lock);
  return pos;
}

void
syscall_tell (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  unsigned pos = -1;

  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    pos = tell (fd);
  }
  f->eax = pos;
}

void
close (int fd)
{
  struct thread *t = thread_current ();
  struct file *cur_file = 0;
  struct thread_fd *cur = get_fd (t, fd);
  if (cur && cur->mmap_id == -1)
  {
    if (cur->mmap_id == -1)
    {
      list_remove (&cur->elem);
      cur_file = cur->t_file;
      lock_acquire (&filesys_lock);
      file_close (cur_file);
      lock_release (&filesys_lock);
      free (cur);
    }
    else
      cur->t_fd = -1;
  }
}

void
close_all_files ()
{
  struct list_elem *e;
  struct thread_fd *cur;
  struct file *cur_file;

  lock_acquire (&filesys_lock);
  struct thread *t = thread_current ();
  while (!list_empty (&t->fd_list))
  {
    e = list_pop_front (&t->fd_list);
    cur = list_entry (e, struct thread_fd, elem);
    if (cur->mmap_id != -1)
      munmap_with_fd (cur);
    ASSERT (cur->t_file != NULL);
    if (!is_file_removed (cur->t_file))
      file_close (cur->t_file);
    free (cur);
  }
  lock_release (&filesys_lock);
}

void
close_all_child_files ()
{
  struct list_elem *e;
  struct child_status *cur;
  struct thread *t = thread_current ();

  while (!list_empty (&t->child_list))
  {
    e = list_pop_front (&t->child_list);
    cur = list_entry (e, struct child_status, elem);
    free (cur);
  }
}

void
syscall_close (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  if (is_valid_pointer (sp))
  {
    fd = *((int *)sp);
    close (fd);
  }
}

mapid_t
mmap (int fd, void *map_addr)
{
  struct thread_fd *cur_fd = get_fd (thread_current (), fd);
  int bytes_read = 0;
  void *addr = map_addr;
  int cur_filesize = filesize (fd);

  if (cur_fd && cur_fd->t_file != NULL
    && cur_fd->mmap_id == -1 && cur_filesize != 0)
  {
    if (addr != NULL && ((int )addr % PGSIZE == 0))
    {
      while (bytes_read < cur_filesize)
      {
        int bytes_i = PGSIZE < (cur_filesize - bytes_read) ? PGSIZE : (cur_filesize - bytes_read);
        if (is_valid_page (addr, NULL) != NULL)
          return -1;
        bytes_read += bytes_i;
        addr += PGSIZE;
      }
      bytes_read = 0;
      addr = map_addr;
      while (bytes_read < cur_filesize)
      {
        int bytes_i = PGSIZE < (cur_filesize - bytes_read) ? PGSIZE : (cur_filesize - bytes_read);
        if (add_page (addr, MMAP, NULL, bytes_read, bytes_i, true, cur_fd->t_file) == NULL)
          return -1;
        bytes_read += bytes_i;
        addr += PGSIZE;
      }
      cur_fd->mmap_id = cur_fd->t_fd;

      lock_acquire (&filesys_lock);
      file_allow_write (cur_fd->t_file);
      lock_release (&filesys_lock);

      return cur_fd->mmap_id;
    }
  }
  //printf ("mmap : %p, filesize : %d\n", cur_fd, cur_filesize);
  return -1;
}

void
syscall_mmap (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  void *addr;
  if (is_valid_pointer (sp))
  {
    fd = *((int *) sp);
    sp += sizeof (int);
    if (is_valid_pointer (sp))
    {
      addr = *((int *) sp);
      f->eax = mmap (fd, addr);
      return;
    }
  }
}

void
munmap_with_fd (struct thread_fd *cur_fd)
{
  if (cur_fd)
  {
    munmap_write_files (cur_fd->t_file);
    cur_fd->mmap_id = -1;
  }
}

void
munmap (mapid_t mapping)
{
  struct thread_fd *cur_fd = get_fd (thread_current (), mapping);
  if (cur_fd)
  {
    //printf ("unmapping file : %p\n", cur_fd->t_file);
    munmap_write_files (cur_fd->t_file);
    if (cur_fd->t_fd == -1)
    {
      close (mapping);
    }
    else
      cur_fd->mmap_id = -1;
  }
}

void
syscall_munmap (struct intr_frame *f)
{
  void *sp = f->esp;
  sp += sizeof (int);

  int fd;
  if (is_valid_pointer (sp))
  {
    fd = *((int *) sp);
    munmap (fd);
  }
}

void
syscall_handler (struct intr_frame *f) 
{
  void *sp = f->esp;
  int syscall_number = -1;

  if (is_valid_pointer (sp))
  {
    syscall_number = *((int *)sp);
  }
  else
    exit (-1);

  switch (syscall_number){
    case SYS_HALT:
      syscall_halt (f);
      break;
    case SYS_EXIT:
      syscall_exit (f);
      break;
    case SYS_EXEC:
      syscall_exec (f);
      break;
    case SYS_WAIT:
      syscall_wait (f);
      break;
    case SYS_CREATE:
      syscall_create (f);
      break;
    case SYS_REMOVE:
      syscall_remove (f);
      break;
    case SYS_OPEN:
      syscall_open (f);
      break;
    case SYS_FILESIZE:
      syscall_filesize (f);
      break;
    case SYS_READ:
      syscall_read (f);
      break;
    case SYS_WRITE:
      syscall_write (f);
      break;
    case SYS_SEEK:
      syscall_seek (f);
      break;
    case SYS_TELL:
      syscall_tell (f);
      break;
    case SYS_CLOSE:
      syscall_close (f);
      break;
    case SYS_MMAP:
      syscall_mmap (f);
      break;
    case SYS_MUNMAP:
      syscall_munmap (f);
      break;
  }
}
