#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

void syscall_init (void);

void close_all_files ();
void close_all_child_files ();

void get_filesys_lock ();
void release_filesys_lock ();

#endif /* userprog/syscall.h */